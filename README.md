> Package is not published

# rollup-plugin-lit-css

Resolve css modules in rollup.

## Install
```bash
npm install @chiogen/rollup-plugin-lit-css
```

## Example
```javascript
// rollup.config.js
import litCss from '@chiogen/rollup-plugin-lit-css'
export default {
    plugins: [
        resolve(),
        litCss()
    ]
}
```