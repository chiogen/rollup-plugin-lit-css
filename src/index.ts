import { promises as fs } from 'fs';
import { Plugin } from 'rollup'
import { createFilter } from '@rollup/pluginutils';

export interface LitCssOptions {
    include?: Array<string | RegExp> | string | RegExp | null
    exclude?: Array<string | RegExp> | string | RegExp | null
    includePaths?: string[]
}


const defaultInclude = '**/*.css';


export default function litScss(options: LitCssOptions = {}): Plugin {

    if (!options.include) {
        options.include = [
            defaultInclude
        ];
    }

    // `options.include` and `options.exclude` can each be a minimatch
    // pattern, or an array of minimatch patterns, relative to process.cwd()
    const filter = createFilter(options.include, options.exclude);

    return {

        name: 'plugin-lit-css',

        async load(id) {
            if (!filter(id)) return null;

            const css = await fs.readFile(id, {
                encoding: 'utf8'
            });

            return wrapModule(css);
        }
    };
}

function wrapModule(css: string) {
    return `import { css } from 'lit'; export default css\`${css}\`;`;
}